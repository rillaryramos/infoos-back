package com.informatica.InfoOS.service;

import com.informatica.InfoOS.models.Usuarios;
import com.informatica.InfoOS.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private BCryptPasswordEncoder pe;

    public Usuarios salvar(Usuarios usuario) {
        usuario.setSenha(pe.encode(usuario.getSenha()));
        return usuarioRepository.save(usuario);
    }

    public Boolean alterarSenha(Usuarios usuario, String senhaAtual, String novaSenha) {
        if(pe.matches(senhaAtual, usuario.getSenha())) {
            usuario.setSenha(pe.encode(novaSenha));
            usuarioRepository.save(usuario);
            return true;
        } else {
            return false;
        }
    }
}
