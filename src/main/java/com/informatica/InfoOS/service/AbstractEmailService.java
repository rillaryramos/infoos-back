package com.informatica.InfoOS.service;

import com.informatica.InfoOS.models.Usuarios;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;

import java.util.Date;

public abstract class AbstractEmailService implements EmailService{

    @Value("${default.sender}")
    private String sender;


    @Override
    public void sendNewPasswordEmail(Usuarios usuario, String newPass) {
        SimpleMailMessage sm = prepareNewPasswordEmail(usuario, newPass);
        sendEmail(sm);
    }

    @Override
    public void sendNewUsuario(Usuarios usuario, String newPass) {
        SimpleMailMessage sm = prepareNewUsuario(usuario, newPass);
        sendEmail(sm);
    }

    protected SimpleMailMessage prepareNewPasswordEmail(Usuarios usuario, String newPass) {
        SimpleMailMessage sm = new SimpleMailMessage();
        sm.setTo(usuario.getEmail());
        sm.setFrom(sender);
        sm.setSubject("Solicitação de nova senha");
        sm.setSentDate(new Date(System.currentTimeMillis()));
        sm.setText("Nova senha: " + newPass);
        return sm;
    }

    protected SimpleMailMessage prepareNewUsuario(Usuarios usuario, String newPass) {
        SimpleMailMessage sm = new SimpleMailMessage();
        sm.setTo(usuario.getEmail());
        sm.setFrom(sender);
        sm.setSubject("Seu cadastro foi realizado no aplicativo InfoOs");
        sm.setSentDate(new Date(System.currentTimeMillis()));
        sm.setText("Acesse o aplicativo com seu e-mail e sua senha: " + newPass);
        return sm;
    }
}
