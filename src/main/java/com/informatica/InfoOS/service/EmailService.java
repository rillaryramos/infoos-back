package com.informatica.InfoOS.service;

import com.informatica.InfoOS.models.Usuarios;
import org.springframework.mail.SimpleMailMessage;

public interface EmailService {

    void sendEmail(SimpleMailMessage msg);

    void sendNewPasswordEmail(Usuarios usuario, String newPass);

    void sendNewUsuario(Usuarios usuario, String newPass);
}
