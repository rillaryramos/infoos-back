package com.informatica.InfoOS.service;

import com.informatica.InfoOS.models.Usuarios;
import com.informatica.InfoOS.repository.UsuarioRepository;
import com.informatica.InfoOS.security.UserSS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UsuarioRepository repo;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Usuarios usuario = repo.findByEmail(email);
        if (usuario == null) {
            throw new UsernameNotFoundException(email);
        }
        return new UserSS(Integer.parseInt(usuario.getId().toString()), usuario.getEmail(), usuario.getSenha(), usuario.isAtivo(), usuario.getPerfis());
    }
}
