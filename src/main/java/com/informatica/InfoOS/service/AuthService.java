package com.informatica.InfoOS.service;

import com.informatica.InfoOS.models.Usuarios;
import com.informatica.InfoOS.repository.UsuarioRepository;
import com.informatica.InfoOS.service.exceptions.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class AuthService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private BCryptPasswordEncoder pe;

    @Autowired
    private EmailService emailService;

    private Random rand = new Random();

    public void sendNewPassword(String email) {
        Usuarios usuario = usuarioRepository.findByEmail(email);
        if(usuario == null) {
            throw new ObjectNotFoundException("Email não encontrado");
        }

        String newPass = newPassword();
        usuario.setSenha(pe.encode(newPass));
        usuarioRepository.save(usuario);
        emailService.sendNewPasswordEmail(usuario, newPass);
    }

    public String newPassword() {
        char[] vet = new char[10];
        for (int i=0; i<10; i++) {
            vet[i] = randomChar();
        }
        return new String(vet);
    }

    public char randomChar() {
        int opt = rand.nextInt(3);
        if (opt == 0) { // gera um dígito
            return (char) (rand.nextInt(10) + 48);
        } else if (opt == 1) { // gera letra maiúscula
            return (char) (rand.nextInt(26) + 65);
        } else { // gera letra minúscula
            return (char) (rand.nextInt(26) + 97);
        }
    }
}
