
package com.informatica.InfoOS.models;

import com.informatica.InfoOS.util.DateTimeUtil;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
public class OrdemDeServico implements Serializable{
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    @NotNull
    private int status;  //Fazer enum
    @NotNull
    private LocalDate dataInicio;
    @NotNull
    private LocalDate dataFinal;
    @NotNull
    private String equipamento;
    @NotNull
    private String modelo;
    @NotNull
    private String marca;
    @NotNull
    private String defeitoInformado;
    @NotNull
    private String servicoRealizado;
    private String observacao;
    @NotNull
    private BigDecimal valorProdutos;
    @NotNull
    private BigDecimal valorMaoDeObra;

    @NotNull
    @ManyToOne
    private Cliente cliente;
    
    @NotNull
    @ManyToOne
    private Usuarios usuario;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDataInicio() {
        return DateTimeUtil.dataString(dataInicio);
    }

    public void setDataInicio(String dataInicio) {
        this.dataInicio = DateTimeUtil.converterStringEmData(dataInicio);
    }

    public String getDataFinal() {
        return DateTimeUtil.dataString(dataFinal);
    }

    public void setDataFinal(String dataFinal) {
        this.dataFinal = DateTimeUtil.converterStringEmData(dataFinal);
    }

    public String getEquipamento() {
        return equipamento;
    }

    public void setEquipamento(String equipamento) {
        this.equipamento = equipamento;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getDefeitoInformado() {
        return defeitoInformado;
    }

    public void setDefeitoInformado(String defeitoInformado) {
        this.defeitoInformado = defeitoInformado;
    }

    public String getServicoRealizado() {
        return servicoRealizado;
    }

    public void setServicoRealizado(String servicoRealizado) {
        this.servicoRealizado = servicoRealizado;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Usuarios getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuarios usuario) {
        this.usuario = usuario;
    }

    public BigDecimal getValorProdutos() {
        return valorProdutos;
    }

    public void setValorProdutos(String valorProdutos) {
        this.valorProdutos = new BigDecimal(valorProdutos.replace(",","."));
    }

    public BigDecimal getValorMaoDeObra() {
        return valorMaoDeObra;
    }

    public void setValorMaoDeObra(String valorMaoDeObra) {
        this.valorMaoDeObra = new BigDecimal(valorMaoDeObra.replace(",","."));
    }
}
