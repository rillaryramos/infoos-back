
package com.informatica.InfoOS.repository;

import com.informatica.InfoOS.models.Usuarios;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuarios, Long>{

    Usuarios findByEmail(String email);

    Usuarios findByCpf(String cpf);

    @Query(value = "SELECT * FROM USUARIOS as u WHERE u.ID <> ?1", nativeQuery = true)
    List<Usuarios> listarUsuarios(Long idUsuario);
}
