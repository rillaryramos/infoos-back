
package com.informatica.InfoOS.repository;

import com.informatica.InfoOS.models.OrdemDeServico;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrdemDeServicoRepository extends JpaRepository<OrdemDeServico, Long>  {
   
}
