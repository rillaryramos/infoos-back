package com.informatica.InfoOS.config;

import com.informatica.InfoOS.service.EmailService;
import com.informatica.InfoOS.service.MockEmailService;
import com.informatica.InfoOS.service.SmtpEmailService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("test")
public class TestConfig {

//    @Bean
//    public EmailService emailService() {
//        return new MockEmailService();
//    }

    @Bean
    public EmailService emailService() {
        return new SmtpEmailService();
    }
}
