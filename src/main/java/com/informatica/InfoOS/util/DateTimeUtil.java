package com.informatica.InfoOS.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateTimeUtil {

    public static String dataString(LocalDate data) {
        String dataString = data.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        return dataString;
    }

    public static LocalDate converterStringEmData(String data) {
        DateTimeFormatter formatador = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate resultado = LocalDate.parse(data, formatador);
        return resultado;
    }
}
