
package com.informatica.InfoOS.controllers;

import com.informatica.InfoOS.controllers.exception.StandardError;
import com.informatica.InfoOS.models.Cliente;
import com.informatica.InfoOS.repository.ClienteRepository;
import com.informatica.InfoOS.util.DateTimeUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.net.URI;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value="/cliente")
@Api(value="API REST Clientes")
public class ClienteController {
    
    @Autowired
    private ClienteRepository clienteRepository;
    
    @ApiOperation(value="Salva um cliente")
    @PostMapping
    public ResponseEntity<Object> salvar(@RequestBody Cliente cliente){
        Cliente verificarDocumento = clienteRepository.findByDocumento(cliente.getDocumento());
        if(verificarDocumento == null) {
            if (DateTimeUtil.converterStringEmData(cliente.getPessoa().getDataNasc()).isBefore(LocalDate.now())) {
                cliente.getPessoa().setDataCadastro(LocalDate.now());
                Cliente clienteNew = clienteRepository.save(cliente);
                URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                        .path("/{id}").buildAndExpand(clienteNew.getId()).toUri();
                return ResponseEntity.created(uri).body(clienteNew);
            }
            StandardError err = new StandardError(System.currentTimeMillis(), HttpStatus.BAD_REQUEST.value(), "Data de nascimento/criação não pode ser igual ou posterior a data atual", "Data de nascimento/criação não pode ser igual ou posterior a data atual", "http://localhost:8080/cliente");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
        }
        StandardError err = new StandardError(System.currentTimeMillis(), HttpStatus.NOT_FOUND.value(), "CPF/CNPJ já cadastrado", "CPF/CNPJ já cadastrado", "http://localhost:8080/cliente");
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(err);
    }
    
    @ApiOperation(value="Lista todos clientes")
    @GetMapping
    public ResponseEntity<List<Cliente>> listar(){
        List<Cliente> clientes = clienteRepository.findAll();
        return ResponseEntity.ok().body(clientes);
    }
    
    @ApiOperation(value="Lista um unico cliente")
    @GetMapping("/{id}")
     public ResponseEntity<Optional<Cliente>> listarUnico(@PathVariable(value="id") Long id){
        Optional<Cliente> cliente = clienteRepository.findById(id);
        return cliente.isPresent() ? ResponseEntity.ok().body(cliente) : ResponseEntity.notFound().build();
    }
    
    @ApiOperation(value="Deleta um cliente")
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletar(@PathVariable(value="id") Long id){
        clienteRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }
    
    @ApiOperation(value="Atualiza um cliente")
    @PutMapping
    public ResponseEntity<Object> atualizar(@RequestBody Cliente cliente){
        if(clienteRepository.findById(cliente.getId()).isPresent()) {
            if(DateTimeUtil.converterStringEmData(cliente.getPessoa().getDataNasc()).isBefore(LocalDate.now())) {
                Cliente clienteAlt = clienteRepository.save(cliente);
                return ResponseEntity.ok(clienteAlt);
            }
            StandardError err = new StandardError(System.currentTimeMillis(), HttpStatus.BAD_REQUEST.value(), "Data de nascimento/criação não pode ser igual ou posterior a data atual", "Data de nascimento/criação não pode ser igual ou posterior a data atual", "http://localhost:8080/cliente");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

}
