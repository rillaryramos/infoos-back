
package com.informatica.InfoOS.controllers;

import com.informatica.InfoOS.controllers.exception.StandardError;
import com.informatica.InfoOS.models.OrdemDeServico;
import com.informatica.InfoOS.models.Usuarios;
import com.informatica.InfoOS.repository.OrdemDeServicoRepository;
import com.informatica.InfoOS.repository.UsuarioRepository;
import com.informatica.InfoOS.util.DateTimeUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.net.URI;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value="/ordemdeservico")
@Api(value="API REST Ordens de Serviço")
public class OrdemDeServicoController {
    
    @Autowired
    private OrdemDeServicoRepository osRepository;
    @Autowired
    private UsuarioRepository usuarioRepository;
    
    @ApiOperation(value="Salva um OS")
    @PostMapping
    public ResponseEntity<Object> salvar(@RequestBody OrdemDeServico os){
        if(!DateTimeUtil.converterStringEmData(os.getDataInicio()).isBefore(LocalDate.now())) {
            if(!DateTimeUtil.converterStringEmData(os.getDataFinal()).isBefore(DateTimeUtil.converterStringEmData(os.getDataInicio()))) {
                Authentication authentication = (Authentication) SecurityContextHolder.getContext().getAuthentication();
                if (authentication != null) {
                    Usuarios usuarioSessao = usuarioRepository.findByEmail(authentication.getName());
                    os.setUsuario(usuarioSessao);
                    os.setStatus(1);
                    OrdemDeServico ordemNew = osRepository.save(os);
                    URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                            .path("/{id}").buildAndExpand(ordemNew.getId()).toUri();
                    return ResponseEntity.created(uri).body(ordemNew);
                }
                return ResponseEntity.badRequest().build();
            }
            StandardError err = new StandardError(System.currentTimeMillis(), HttpStatus.BAD_REQUEST.value(), "Data de término não pode ser anterior a data de início", "Data de término não pode ser anterior a data de início", "http://localhost:8080/ordemdeservico");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
        }
        StandardError err = new StandardError(System.currentTimeMillis(), HttpStatus.BAD_REQUEST.value(), "Data de início não pode ser anterior a data atual", "Data de início não pode ser anterior a data atual", "http://localhost:8080/ordemdeservico");
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
    }
    
    @ApiOperation(value="Lista todas OS")
    @GetMapping
    public ResponseEntity<List<OrdemDeServico>> listar(){
        List<OrdemDeServico> ordens = osRepository.findAll();
        return ResponseEntity.ok().body(ordens);
    }
    
    @ApiOperation(value="Lista uma unica OS")
    @GetMapping("/{id}")
     public ResponseEntity<Optional<OrdemDeServico>> listarUnico(@PathVariable(value="id") Long id){
        Optional<OrdemDeServico> ordem = osRepository.findById(id);
        return ordem.isPresent() ? ResponseEntity.ok().body(ordem) : ResponseEntity.notFound().build();
    }
    
    @ApiOperation(value="Deleta um cliente")
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletar(@PathVariable(value="id") Long id){
        osRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }
    
    @ApiOperation(value="Atualiza um cliente")
    @PutMapping
    public ResponseEntity<Object> atualizar(@RequestBody OrdemDeServico os){
        if(osRepository.findById(os.getId()).isPresent()) {
            if(!DateTimeUtil.converterStringEmData(os.getDataInicio()).isBefore(LocalDate.now())) {
                if(!DateTimeUtil.converterStringEmData(os.getDataFinal()).isBefore(DateTimeUtil.converterStringEmData(os.getDataInicio()))) {
                    OrdemDeServico ordemAlt = osRepository.save(os);
                    return ResponseEntity.ok(ordemAlt);
                }
                StandardError err = new StandardError(System.currentTimeMillis(), HttpStatus.BAD_REQUEST.value(), "Data de término não pode ser anterior a data de início", "Data de término não pode ser anterior a data de início", "http://localhost:8080/ordemdeservico");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
            }
            StandardError err = new StandardError(System.currentTimeMillis(), HttpStatus.BAD_REQUEST.value(), "Data de início não pode ser anterior a data atual", "Data de início não pode ser anterior a data atual", "http://localhost:8080/ordemdeservico");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/alterar-status/{idOrdem}")
    public ResponseEntity<Void> alterarSituacao(@PathVariable("idOrdem") Long idOrdem, @RequestParam("tipo") int tipo) {
        OrdemDeServico ordem = osRepository.getOne(idOrdem);
        System.out.println("teste: " + ordem.getDefeitoInformado());
        if((ordem != null) && (tipo > 0) && (tipo < 4)) {
            ordem.setStatus(tipo);
            osRepository.save(ordem);
            System.out.println("teste2: " + ordem.getStatus());
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().build();
    }
    
}
