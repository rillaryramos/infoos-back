
package com.informatica.InfoOS.controllers;

import com.informatica.InfoOS.controllers.exception.StandardError;
import com.informatica.InfoOS.models.Usuarios;
import com.informatica.InfoOS.repository.UsuarioRepository;
import com.informatica.InfoOS.service.AuthService;
import com.informatica.InfoOS.service.EmailService;
import com.informatica.InfoOS.service.UsuarioService;
import com.informatica.InfoOS.util.DateTimeUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.net.URI;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpSession;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value="/usuario")
@Api(value="API REST Usuarios")
public class UsuarioController {
    
    @Autowired
    private UsuarioRepository usuarioRepository;
    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private AuthService authService;
    @Autowired
    private BCryptPasswordEncoder pe;
    @Autowired
    private EmailService emailService;

    @PreAuthorize("hasAnyRole('ADMIN')")
    @ApiOperation(value="Salva um usuario")
    @PostMapping
    public ResponseEntity<Object> salvar(@RequestBody Usuarios usuario){

        Usuarios verificarEmail = usuarioRepository.findByEmail(usuario.getEmail());
        Usuarios verificarCpf = usuarioRepository.findByCpf(usuario.getCpf());
        System.out.println(verificarCpf);
        if(verificarEmail == null) {
            if(verificarCpf == null) {
                if(DateTimeUtil.converterStringEmData(usuario.getPessoa().getDataNasc()).isBefore(LocalDate.now())) {
                    String newPass = authService.newPassword();
                    usuario.setSenha(pe.encode(newPass));
                    usuario.setAtivo(true);
                    usuario.setGerente(false); //tirar o gerente
                    usuario.getPessoa().setDataCadastro(LocalDate.now());
                    Usuarios usuarioNew = usuarioRepository.save(usuario);
                    emailService.sendNewUsuario(usuario, newPass);

                    URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                            .path("/{id}").buildAndExpand(usuarioNew.getId()).toUri();
                    return ResponseEntity.created(uri).body(usuarioNew);
                }
                StandardError err = new StandardError(System.currentTimeMillis(), HttpStatus.BAD_REQUEST.value(), "Data de nascimento não pode ser igual ou posterior a data atual", "Data de nascimento não pode ser igual ou posterior a data atual", "http://localhost:8080/usuario");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
            }
            StandardError err = new StandardError(System.currentTimeMillis(), HttpStatus.NOT_FOUND.value(), "CPF já cadastrado", "CPF já cadastrado", "http://localhost:8080/usuario");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(err);
        }
        StandardError err = new StandardError(System.currentTimeMillis(), HttpStatus.NOT_FOUND.value(), "E-mail já cadastrado", "E-mail já cadastrado", "http://localhost:8080/usuario");
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(err);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @ApiOperation(value="Lista todos usuarios")
    @GetMapping
    public ResponseEntity<List<Usuarios>> listar(){
        Authentication authentication = (Authentication) SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            Usuarios usuarioSessao = usuarioRepository.findByEmail(authentication.getName());
            List<Usuarios> usuarios = usuarioRepository.listarUsuarios(usuarioSessao.getId());
            return ResponseEntity.ok().body(usuarios);
        }
        return ResponseEntity.badRequest().build();
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @ApiOperation(value="Lista um unico usuario")
    @GetMapping("/{id}")
     public ResponseEntity<Optional<Usuarios>> listarUnico(@PathVariable(value="id") Long id){
        Optional<Usuarios> usuario = usuarioRepository.findById(id);
        return usuario.isPresent() ? ResponseEntity.ok().body(usuario) : ResponseEntity.notFound().build();
    }

    @GetMapping("/email")
    public ResponseEntity<Usuarios> find(@RequestParam(value="value") String email) {
        Usuarios usuario = usuarioRepository.findByEmail(email);
        return ResponseEntity.ok().body(usuario);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @ApiOperation(value="Deleta um usuario")
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletar(@PathVariable(value="id") Long id){
        usuarioRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }
    
    @ApiOperation(value="Atualiza um usuario")
    @PutMapping
    public ResponseEntity<Object> atualizar(@RequestBody Usuarios usuario){
        Usuarios usu = usuarioRepository.getOne(usuario.getId());
        if(usu != null) {
            if(DateTimeUtil.converterStringEmData(usuario.getPessoa().getDataNasc()).isBefore(LocalDate.now())) {
                usu.getPessoa().setDataNasc(usuario.getPessoa().getDataNasc());
                usu.getPessoa().setNome(usuario.getPessoa().getNome());
                usu.getPessoa().setTelefone(usuario.getPessoa().getTelefone());
                usu.getPessoa().getEndereco().setComplemento(usuario.getPessoa().getEndereco().getComplemento());
                usu.getPessoa().getEndereco().setNumero(usuario.getPessoa().getEndereco().getNumero());
                usu.getPessoa().getEndereco().setEndereco(usuario.getPessoa().getEndereco().getEndereco());
                usu.getPessoa().getEndereco().setBairro(usuario.getPessoa().getEndereco().getBairro());
                usu.getPessoa().getEndereco().setCep(usuario.getPessoa().getEndereco().getCep());
                Usuarios usuarioAlt = usuarioRepository.save(usu);
                return ResponseEntity.ok(usuarioAlt);
            }
            StandardError err = new StandardError(System.currentTimeMillis(), HttpStatus.BAD_REQUEST.value(), "Data de nascimento não pode ser igual ou posterior a data atual", "Data de nascimento não pode ser igual ou posterior a data atual", "http://localhost:8080/usuario");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/alterarSenha")
    public ResponseEntity<Object> alterarSenha(@RequestParam(value="senhaAtual") String senhaAtual, @RequestParam(value="novaSenha") String novaSenha) {
        Authentication authentication = (Authentication) SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && senhaAtual != null && novaSenha != null) {
            Usuarios usuario = usuarioRepository.findByEmail(authentication.getName());
            Boolean alterou = usuarioService.alterarSenha(usuario, senhaAtual, novaSenha);
            if(alterou) {
                return ResponseEntity.ok().build();
            } else {
                ResponseEntity.badRequest().build();
                StandardError err = new StandardError(System.currentTimeMillis(), HttpStatus.BAD_REQUEST.value(), "Senha atual informada não corresponde a sua senha atual", "Senha atual informada não corresponde a sua senha atual", "http://localhost:8080/usuario");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
            }
        }
        return ResponseEntity.badRequest().build();
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PutMapping("/status/{idUsuario}")
    public ResponseEntity<Void> ativarInativarUsuario(@PathVariable(value = "idUsuario") Long idUsuario) {
        if(idUsuario > 0) {
            Usuarios usuario = usuarioRepository.getOne(idUsuario);
            if(usuario != null) {
                if (usuario.isAtivo()) {
                    usuario.setAtivo(false);
                } else {
                    usuario.setAtivo(true);
                }
                usuarioRepository.save(usuario);
                return ResponseEntity.ok().build();
            }
        }
        return ResponseEntity.badRequest().build();
    }
    
}
