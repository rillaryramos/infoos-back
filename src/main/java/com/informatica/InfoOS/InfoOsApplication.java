package com.informatica.InfoOS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InfoOsApplication {

	public static void main(String[] args) {
		SpringApplication.run(InfoOsApplication.class, args);
	}
}
